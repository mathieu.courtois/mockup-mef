#define MEFXX_VERSION_MAJOR 0
#define MEFXX_VERSION_MINOR 1

#include "mpi.h"

#include <string>
#include <vector>

using VectorString = std::vector< std::string >;

namespace MEFXX {

void initialisation( VectorString &argv, MPI_Comm comm );

}
