# MEF++ mockup

This project hosts a very simple library used to work on a coupling model
between [code_aster][1] and [MEF++][2].


[1]: https://www.code-aster.org/
[2]: https://fr.wikipedia.org/wiki/MEF%2B%2B
