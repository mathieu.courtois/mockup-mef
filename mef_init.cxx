#include <iostream>

#include "mef++.h"

namespace MEFXX {

void initialisation( VectorString &argv, MPI_Comm comm ) {
    int rank, size;

    std::cout << "MEF++ - initialisation..." << std::endl;
    MPI_Comm_rank( comm, &rank );
    MPI_Comm_size( comm, &size );
    std::cout << "Processus #" << rank << "/" << size << std::endl;

    int i = 0;
    for ( auto &arg : argv ) {
        std::cout << "+ " << i << ": " << arg << std::endl;
        i++;
    }

    std::cout << "MEF++ - fin de l'initialisation" << std::endl;
}

} // namespace MEFXX
