INCLUDES=-I/usr/lib/x86_64-linux-gnu/openmpi/include/openmpi -I/usr/lib/x86_64-linux-gnu/openmpi/include

CXX ?= mpicxx
CXXFLAGS = -fPIC $(INCLUDES)
OBJS = mef_init.o

default: libmef++.so

%.o: %.cxx
	$(CXX) $(CXXFLAGS) -o $@ -c $<

libmef++.so: $(OBJS)
	$(CXX) --shared -fPIC $(OBJS) -o $@

clean:
	rm -f *.o

distclean: clean
	rm -f *.so
